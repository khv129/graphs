#include "../../unittests/unittestframework.h"
#include "../list.h"

class List_Unit_Test : public  Unit_Test
{
    public:
    
    List_Unit_Test(){
        register_test_function(testcase_addElements_to_List,"testcase_addElements_to_List");
        register_test_function(testcase_find_addedElements_to_List_pass,"testcase_find_addedElements_to_List_pass");
        register_test_function(testcase_find_addedElements_to_List_fail,"testcase_find_addedElements_to_List_fail");
        run_tests();
    }
//TestCase 1 : Add Elements to List

   static bool testcase_addElements_to_List(){
       
       List<int> TestList;
       return TestList.add(10);
       
   }
//TestCase 2 : find the added Element in the list

   static bool testcase_find_addedElements_to_List_pass(){
       
       List<int> TestList;
       TestList.add(20);
     
       if(TestList.find(20))
       {
	       if( TestList.find(20)->getData()== 20){
	          return true;
	       }
	       else
	       {
	           return false;
	       }
       } 
       else
	{
		return false;
	}
    }
   
   
//Testcase 3 : find a element which is not present
  static bool testcase_find_addedElements_to_List_fail(){
       
       List<int> TestList;
       TestList.add(20);
       
       if( TestList.find(10)==nullptr)
        {
              return true;
        }
       else
       {
           return false;
       }
  
   }
};

int main()
{
   List_Unit_Test();
}
