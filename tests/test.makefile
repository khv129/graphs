IDIR = ../../includes
ODIR = ../../objects/test
CC = g++
CFLAGS = -std=c++0x -I $(IDIR)

OBJ = $(ODIR)/list.o $(ODIR)/node.o $(ODIR)/unittestframework.o $(ODIR)/listunittest.o   



$(ODIR)/list.o : ../list.cpp  $(DEPS)
	$(CC) ../list.cpp -c -o $(ODIR)/list.o $(CFLAGS) 

$(ODIR)/node.o : ../node.cpp  $(DEPS)
	$(CC) ../node.cpp -c -o $(ODIR)/node.o $(CFLAGS)

$(ODIR)/unittestframework.o : ../../unittests/unittestframework.cpp $(DEPS)
	$(CC)  ../../unittests/unittestframework.cpp -c -o $(ODIR)/unittestframework.o  $(CFLAGS)

$(ODIR)/listunittest.o : listunittest.cpp $(DEPS)
	$(CC)  listunittest.cpp  -c -o $(ODIR)/listunittest.o  $(CFLAGS)

build : $(OBJ)
	$(CC) $(ODIR)/listunittest.o $(ODIR)/unittestframework.o $(ODIR)/node.o $(ODIR)/list.o  $(CFLAGS) -o test.exe 

.PHONY : clean
clean :
	rm -f $(ODIR)/*.o