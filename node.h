#ifndef NODE
#define NODE

#include "globalincludes.h"

template<class type>
class node {
   type data;
   node<type> *llink; //Left Link
   node<type> *rlink; //Right Link
  
  public:
   node(type data,node<type> *llink,node<type> *rlink);
   type getData();
   void linkAtRight(node<type> *link);
   node<type>* getRightLink();
};

template<class type>
type node<type>::getData(){
      return data;
}

template<class type>
void node<type>::linkAtRight(node<type> *link){
         
         rlink = link;
}

template<class type>
node<type>* node<type>::getRightLink(){
         return rlink;
}

template<class type>
node<type>::node(type data,node<type> *llink,node<type> *rlink):
    data(data),
    llink(llink),
    rlink(rlink) {        
}

#endif