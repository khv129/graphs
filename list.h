#ifndef LIST
#define LIST

#include "globalincludes.h"
#include "node.h"

template<class type>
class List {
   node<type> *head;
   node<type> *tail;

   public:
    List();   
    bool add(type d); 
    node<type>* find(type d);
   ~List();
};

template<class type>
List<type>::List():
    head(nullptr),
    tail(nullptr)
{
   
}

template<class type>
bool List<type>::add(type d){
     
     node<type> *temp_node = nullptr;
     if(!tail && !head) {
        
        temp_node = new node<type>(d,tail,head);
        tail = head = temp_node;
     }
     else
     {
        temp_node = new node<type>(d,tail,nullptr);
        tail->linkAtRight(temp_node);     
     }
     return temp_node;
 }

template<class type>
node<type>* List<type>::find(type d){
     
     node<type> *temp_node = nullptr;
     
     if(head){
         
         temp_node = head;
         
         while( temp_node!=nullptr && temp_node->getData() != d){
              temp_node = temp_node->getRightLink();
         }
         
         if(temp_node!= nullptr && temp_node->getData() == d){
              return temp_node;    
         }
     }
     
     return nullptr;
 }

template<class type>
List<type>::~List(){
      node<type> *temp_node = nullptr;
      node<type> *temp_next_node = nullptr; 
     if(head){
         
         temp_node = head;
         
         while(temp_node!=nullptr){

             temp_next_node = temp_node->getRightLink();
             delete temp_node;
             temp_node = temp_next_node;
              
         }
         
     }
 }
#endif
